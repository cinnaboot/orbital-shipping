
#include <GL/glew.h>

#include <imgui.h>
#include <backends/imgui_impl_opengl3.h>
#include <backends/imgui_impl_sdl.h>
using namespace ImGui;

#include "gooey.h"
#include "orbits.h"
#include "util.h"


const static int G_WIDTH = 400;
const static ImVec2 V_SPACER(0, 10);
const int H_FLAGS = ImGuiTreeNodeFlags_DefaultOpen;

// forward declarations

void drawSimulationWindow(bool& running, u64 sim_time_ms, float& sim_speed);
void drawMemoryWindow(MemoryArena* game_arena, RenderState* rs);
void drawOrbitSelectionWindow(GameState* gs);
void drawEllipseParameters(EllipseParameters& ep);
void drawGravitationalBody(GravBody& body);
void drawOrbitalElements(OrbitalElements& el);
void drawSatelliteWindow(Satellite& sat);
void drawSystemWindow(TwoBodySystem& sys);
void drawManeuverWindow(GameState* gs);


// interface

bool
gooInit(SDL_Window* window, SDL_GLContext gl_context)
{
	IMGUI_CHECKVERSION();
	CreateContext();
	ImGuiIO& io = GetIO();
	io.IniFilename = NULL;  // don't save window state to imgui.ini
	StyleColorsDark();

	bool ret = true;
	ret = ret && ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
	ret = ret && ImGui_ImplOpenGL3_Init();

	return ret;
}

void
gooFree()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	DestroyContext();
}

bool
gooProcessEvent(SDL_Event& e)
{
	ImGui_ImplSDL2_ProcessEvent(&e);
	return (GetIO().WantCaptureMouse
		|| GetIO().WantCaptureKeyboard);
}

void
gooDraw(SDL_Window* window, GameState* gs, RenderState* rs)
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(window);
	NewFrame();

	int x = 0, y = 0;
	SDL_GetWindowSize(window, &x, &y);
	SetNextWindowPos(ImVec2(x - G_WIDTH, 0));
	SetNextWindowSize(ImVec2(G_WIDTH, y));
	Begin("Gooey");

	drawSimulationWindow(gs->running, gs->sim_time_ms, gs->sim_speed);
	drawMemoryWindow(gs->arena, rs);
	Separator();

	drawOrbitSelectionWindow(gs);
	GameOrbit* orbit = getSelectedOrbit(gs);

	if (orbit) {
		TwoBodySystem& sys = orbit->system;
		BeginChild("orbit details", ImVec2(G_WIDTH - 20, 450), true);
		drawSystemWindow(sys);
		Separator();
		drawSatelliteWindow(sys.sat);
		Separator();
		drawOrbitalElements(sys.elements);
		EndChild();
	}

	BeginChild("impulsive maneuver", ImVec2(G_WIDTH - 20, 125), true);
	drawManeuverWindow(gs);
	EndChild();

	End();
	Render();
	ImGui_ImplOpenGL3_RenderDrawData(GetDrawData());
}


// internal

void drawSimulationWindow(bool& running, u64 sim_time_ms, float& sim_speed)
{
	if (Button("||")) running = false;
	SameLine();
	if (Button("|>")) running = true;
	SameLine();
	Text("running: %s", (running > 0) ? "true" : "false");

	Text("sim time:			%.2f s",
				double(double(sim_time_ms) / 1000));
	InputFloat("sim speed", &sim_speed, 0.1, 4.0, "%.1f");
}

void drawMemoryWindow(MemoryArena* game_arena, RenderState* rs)
{
	MemoryArena* rg_arena = rs->rg_arena;
	MemoryArena* asset_arena = rs->assets.arena;

	BeginChild("memory", ImVec2(G_WIDTH - 20, 125), true);
		Text("GameState Memory Arena");
		float freeMB = (float) game_arena->free_size / (float) (1024 * 1024);
		float maxMB = (float) game_arena->max_size / (float) (1024 * 1024);
		float usedMB = maxMB - freeMB;
		Text("%.2f MB", usedMB);
		SameLine();
		ProgressBar(usedMB / maxMB, ImVec2(100, 15));
		SameLine();
		Text("%.2f MB", maxMB);

		Text("RenderGroup Memory Arena");
		freeMB = (float) rg_arena->free_size / (float) (1024 * 1024);
		maxMB = (float) rg_arena->max_size / (float) (1024 * 1024);
		usedMB = maxMB - freeMB;
		Text("%.2f MB", usedMB);
		SameLine();
		ProgressBar(usedMB / maxMB, ImVec2(100, 15));
		SameLine();
		Text("%.2f MB", maxMB);

		Text("Asset Memory Arena");
		freeMB = (float) asset_arena->free_size / (float) (1024 * 1024);
		maxMB = (float) asset_arena->max_size / (float) (1024 * 1024);
		usedMB = maxMB - freeMB;
		Text("%.2f MB", usedMB);
		SameLine();
		ProgressBar(usedMB / maxMB, ImVec2(100, 15));
		SameLine();
		Text("%.2f MB", maxMB);
	EndChild();
}

void drawOrbitSelectionWindow(GameState* gs)
{
	Text("Orbit Selection");

	BeginChild("orbit selection", ImVec2(G_WIDTH - 20, 55), true);

	for (u32 i = 0; i < gs->num_orbits; i++) {
		GameOrbit& orbit = gs->orbits[i];

		if (orbit.in_use) {
			const u32 label_len = 32;
			static char label[label_len];
			snprintf(label, label_len, "Orbit %d", i);

			if (Selectable(label, orbit.selected))
				selectOrbit(gs, &orbit);
		}
	}

	EndChild();
}

void drawEllipseParameters(EllipseParameters& ep)
{
	Text("semi_major axis, a:     %f km", ep.a);
	Text("semi_minor axis, b:     %f km", ep.b);
	Text("eccentricity, e:        %f   ", ep.e);
	Text("linear eccentricity, c: %f km", ep.c);
	Text("semilatus rectum, p:    %f km", ep.p);
}

void drawGravitationalBody(GravBody& body)
{
	Text("Grav Body:");
	Indent();
	Text("mu, gravitational parameter: %.3f", body.mu);
	Text("r, radius in km:             %.3f", body.radius);
	Unindent();
}

void drawOrbitalElements(OrbitalElements& el)
{
	Text("Orbital Elements:");
	Indent();
		Text("semi_major axis, a:         %f km", el.a);
		Text("eccentricity, e:               %f", el.e);
		Text("iota, inclination:             %f", el.iota);
		Text("ohm, longitude ascending node: %f", el.ohm);
		Text("omega, argument of periapsis:  %f", el.omega);
		Text("nu, true anomaly at T0:        %f", el.nu);
	Unindent();
}

void
drawSatelliteWindow(Satellite& sat)
{
	Text("Satellite Parameters:");
	Indent();
		Text("theta, true anomaly: %f", sat.theta);
		Text("r, radial distance:  %f km", sat.r);
		Text("position:");
	Indent();
			Text("x: %f km", sat.position.x);
			Text("y: %f km", sat.position.y);
			Text("z: %f km", sat.position.z);
	Unindent();
		Text("velocity, km/s:      %f", sat.v);
		Text("flight path angle:   %f", sat.gamma);
	Unindent();
}

void drawSystemWindow(TwoBodySystem& sys)
{
	Text("System Info:");
	Indent();
		Text("orbital period:       %.2f s", sys.orbital_period);
		Text("epsilon, spec. orb. energy: %f", sys.epsilon);
		Text("h, angular momentum:        %f", sys.h);
		Text("r_apoapsis:           %f km", sys.r_apoapsis);
		Text("r_periapsis:          %f km", sys.r_periapsis);
		// FIXME: not ideal to call into orbit interface from the gooey
		double tof_apoapse = orbitGetTimeOfFlight(sys, sys.sat.theta, M_PI);
		Text("time to apoapsis:     %.2f s", tof_apoapse);
		double tof_periapse =
			orbitGetTimeOfFlight(sys, sys.sat.theta, 2 * M_PI);
		Text("time to periapsis:    %.2f s", tof_periapse);
	Unindent();
}

void
drawManeuverWindow(GameState* gs)
{
	GameOrbit* orbit = getSelectedOrbit(gs);
	static int item_current_idx = 0;
	static double anom = 0.f;
	static float dv = 0.f;

	if (orbit) {
		if (orbit->last_maneuver) {
			Text("Orbital Maneuver");
			Indent();
			Text("true anomaly: %f", orbit->first_maneuver->true_anomaly);
			Text("impulse dv: %f km/s", orbit->first_maneuver->impulse_delta_v);
			Unindent();

			if (Button("remove")) {
				removeManeuver(orbit, orbit->first_maneuver);
				item_current_idx = 0;
				anom = 0.f;
				dv = 0.f;
			}
		} else {
			Text("Add orbital maneuver");

			const double min_anom = -M_PI, max_anom = M_PI;
			SliderScalar("maneuver anomaly", ImGuiDataType_Double, &anom,
					&min_anom, &max_anom, "%.19f");

			// FIXME: map ImpulseType to strings for UI
			// select maneuver vector
			const char* items[] = {
				"prograde",
				"retrograde",
				"some other vector"
			};

			Combo("thrust vector", &item_current_idx, items,
					IM_ARRAYSIZE(items));

			InputFloat("impulse dv, km/s", &dv, 0.1, 4.0, "%.1f");

			if (Button("apply")) {
				if (!addManeuver(gs, orbit, ImpulseType::PROGRADE, anom, dv)) {
					// flash a color or something?
				}
			}
		}
	}
}
