
#pragma once

#include <SDL2/SDL.h>

#include "game.h"
#include "orbits.h"
#include "tangerine.h"


bool
gooInit(SDL_Window* window, SDL_GLContext gl_context);

void
gooFree();

bool
gooProcessEvent(SDL_Event &e);

void
gooDraw(SDL_Window* window, GameState* gs, RenderState* rs);
