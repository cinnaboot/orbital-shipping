#pragma once

#define GLM_FORCE_XYZW_ONLY
#include <glm/glm.hpp>
using glm::vec3;
using glm::dvec3;
using glm::dmat3;

#include "tangerine.h"
#include "orbits.h"


#define DEFAULT_ORBIT_VERTICES 128

const vec3 DEFAULT_ELLIPSE_COLOR = vec3(0.3, 0.3, 0.3);
const vec3 SELECTED_ELLIPSE_COLOR = vec3(0.7, 0.2, 0.2);


struct Ellipse3D
{
	vec3* vertices;
	uint vert_count;
};

enum struct ImpulseType
{
	NONE,
	PROGRADE,
	RETROGRADE,
	CIRCULARIZE_RAISING,
	CIRCULARIZE_LOWERING,
	INCLINATION_CHANGE,
	GENERAL_PLANE_CHANGE,
	OTHER,
	VECTOR_TYPE_COUNT
};

struct ManeuverNode
{
	ImpulseType impulse_type;
	double true_anomaly;
	double impulse_delta_v;
	bool active;

	ManeuverNode* previous_maneuver;
	ManeuverNode* next_maneuver;
};

struct GameOrbit
{
	TwoBodySystem system;
	Ellipse3D e3d;
	ManeuverNode* first_maneuver;
	ManeuverNode* last_maneuver;

	Entity* grav_body;
	Entity* ellipse_entity;
	Entity* satellite_entity;
	bool in_use;
	bool selected;
};

struct GameState
{
	bool running;
	MemoryArena* arena;

	u64 game_time_ms;
	u64 sim_time_ms;
	float sim_speed;

	GameOrbit* orbits;
	u32 num_orbits;
	u32 max_orbits;

	ManeuverNode* maneuver_nodes;
	u32 max_maneuvers;

	GameOrbit* last_selected_orbit;
	Entity* coord_overlay;
};

GameOrbit* getFreeOrbit(GameState* gs);

GameOrbit* getSelectedOrbit(GameState* gs);

void disableGameOrbit(GameState* gs, GameOrbit* orbit);

double getTimeStep(GameState* gs);

Ellipse3D ellipse3DInit(dmat3 rotation, EllipseParameters ep, uint vert_count);

void ellipse3DUpdate(dmat3 rotation, EllipseParameters ep, Ellipse3D& e3d);

void selectOrbit(GameState* gs, GameOrbit* orbit);

bool addManeuver(GameState* gs,
		GameOrbit* orbit,
		ImpulseType impulse_type,
		double true_anomaly = 0.f,
		double impulse_delta_v = 0.f);

bool testManeuverStep(ManeuverNode* maneuver,
		double previous_theta,
		double next_theta);

void applyManeuver(GameOrbit* orbit, ManeuverNode* node);

void removeManeuver(GameOrbit* orbit, ManeuverNode* maneuver);
