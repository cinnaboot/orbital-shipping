
SHELL := /bin/sh
OBJDIR := build
SRCDIR := src
BINDIR := bin
LIBDIR := ext/tangerine
LIB := $(LIBDIR)/build/libTangerine.a

CXX := g++
# NOTE: due to a longstanding 'bug' in gcc, we can only check for unitialized
# 	variables if an optimization pass is run. However, compiling without
# 	optimization is useful for debugging, so we can add some local variables
# 	for easier inspection.
# 	see) https://gcc.gnu.org/bugzilla/show_bug.cgi?id=18501
CHECK_UNITIALIZED := false
ifeq ($(CHECK_UNITIALIZED), true)
	CXXFLAGS := -std=c++11 -g -ggdb3 -O -Wall
else
	CXXFLAGS := -std=c++11 -g -ggdb3 -Wall
endif
CXXFLAGS += -I$(LIBDIR)/include/ \
			$(shell sdl2-config --cflags)

LDFLAGS := $(shell sdl2-config --libs) -lGLEW -lGL

COMMON_SOURCES := $(SRCDIR)/orbits.cpp
COMMON_OBJECTS := $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(COMMON_SOURCES))

TESTDIR := tests
TEST_SOURCES := $(wildcard $(TESTDIR)/*.cpp)
TEST_OBJECTS := $(patsubst $(TESTDIR)/%.cpp, $(OBJDIR)/%.o, $(TEST_SOURCES))
TEST_BIN := $(BINDIR)/test_orbit

# imgui setup
IMGUI_DIR := ext/imgui
CXXFLAGS += -I$(IMGUI_DIR)
IMGUI_IMPL_DIR := $(IMGUI_DIR)/backends
IMGUI_IMPL_SOURCES := $(IMGUI_IMPL_DIR)/imgui_impl_opengl3.cpp $(IMGUI_IMPL_DIR)/imgui_impl_sdl.cpp
IMGUI_SOURCES := $(filter-out $(IMGUI_DIR)/imgui_demo.cpp, $(wildcard $(IMGUI_DIR)/*.cpp))
IMGUI_OBJECTS := $(patsubst $(IMGUI_DIR)/%.cpp, $(OBJDIR)/%.o, $(IMGUI_SOURCES))
IMGUI_IMP_OBJECTS += $(patsubst $(IMGUI_IMPL_DIR)/%.cpp, $(OBJDIR)/%.o, $(IMGUI_IMPL_SOURCES))

SOURCES := $(SRCDIR)/main.cpp $(SRCDIR)/gooey.cpp $(SRCDIR)/game.cpp
OBJECTS := $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(SOURCES))
BIN := $(BINDIR)/orbital_shipping


all: mkdirs $(BIN) tests tags
	@# automatically run tests when (re-)compiling
	@${TEST_BIN}
.PHONY: all

tests: $(TEST_BIN)
.PHONY: tests

tags:
	@ctags -R --exclude=ext/imgui/* --exclude=ext/tangerine/ext/*
.PHONY: tags

mkdirs:
	@-mkdir -p ./bin ./build
.PHONY: mkdirs

-include $(OBJDIR)/*.d

$(BIN): $(COMMON_OBJECTS) $(OBJECTS) $(IMGUI_OBJECTS) $(IMGUI_IMP_OBJECTS) $(LIB)
	$(CXX) -o $@ $(LDFLAGS) $(COMMON_OBJECTS) $(OBJECTS) $(LIB) \
		$(IMGUI_OBJECTS) $(IMGUI_IMP_OBJECTS)

$(TEST_BIN): $(TEST_OBJECTS) $(LIB)
	$(CXX) -o $@ $(LDFLAGS) $(TEST_OBJECTS) $(LIB)

$(COMMON_OBJECTS): $(COMMON_SOURCES)
	$(CXX) $(CXXFLAGS) -c -MMD $(SRCDIR)/orbits.cpp -o $@

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

$(TEST_OBJECTS): $(OBJDIR)/%.o : $(TESTDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

$(IMGUI_OBJECTS): $(OBJDIR)/%.o : $(IMGUI_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

$(IMGUI_IMP_OBJECTS): $(OBJDIR)/%.o : $(IMGUI_IMPL_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

$(LIB):
	$(MAKE) -C $(LIBDIR)

clean:
	@rm -f $(BIN) $(TEST_BIN) $(OBJDIR)/*
.PHONY: clean

clean_lib:
	$(MAKE) -C $(LIBDIR) clean
.PHONY: clean_lib

